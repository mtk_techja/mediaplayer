package com.klm.mediaplayer;

public class ItemSong {
    private String path, name, album;

    public ItemSong(String path, String name, String album) {
        this.path = path;
        this.name = name;
        this.album = album;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public String getAlbum() {
        return album;
    }
}
