package com.klm.mediaplayer;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    private RecyclerView rvListSong;
    private List<ItemSong> listData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            initViews();
            return;
        }
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 101);
    }

    private void initData() {
        Log.i(TAG, "initData..." + listData);
        if (listData == null) return;

        SongAdapter songAdapter = new SongAdapter(this, listData);

        rvListSong.setLayoutManager(new LinearLayoutManager(this));
        rvListSong.setAdapter(songAdapter);
    }

    private void initViews() {
        rvListSong = findViewById(R.id.rv_song);
        loadSongFromExternalStorage();
        initData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        initViews();
    }

    private void loadSongFromExternalStorage() {
        Log.i(TAG, "loadSongFromExternalStorage...");
        //Buoc1
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        //B2
        String[] projections = new String[]{
                MediaStore.Audio.Media.DISPLAY_NAME,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.DATA,
        };
        Cursor c = getContentResolver().query(uri, projections, null, null, MediaStore.Audio.Media.DISPLAY_NAME + " ASC");
        if (c == null) return;
        c.moveToFirst();
        listData = new ArrayList<>();
        while (!c.isAfterLast()) {
            String name = getCData(c, MediaStore.Audio.Media.DISPLAY_NAME);
            String album = getCData(c, MediaStore.Audio.Media.ALBUM);
            String path = getCData(c, MediaStore.Audio.Media.DATA);

            listData.add(new ItemSong(path, name, album));
            c.moveToNext();
        }
        c.close();
        Log.i(TAG, "Song size..." + listData.size());
    }

    private String getCData(Cursor c, String name) {
        return c.getString(c.getColumnIndex(name));
    }
}
