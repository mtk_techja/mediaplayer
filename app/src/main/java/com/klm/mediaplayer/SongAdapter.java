package com.klm.mediaplayer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongHolder> {
    private Context context;
    private List<ItemSong> listData;

    public SongAdapter(Context context, List<ItemSong> listData) {
        this.context = context;
        this.listData = listData;
    }

    @NonNull
    @Override
    public SongHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_song, parent, false);
        return new SongHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SongHolder holder, int position) {
        ItemSong song = listData.get(position);
        holder.tvTitle.setText(song.getName());
        holder.tvAlbum.setText(song.getAlbum());
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class SongHolder extends RecyclerView.ViewHolder{
        TextView tvTitle, tvAlbum;
        public SongHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle=itemView.findViewById(R.id.tv_title);
            tvAlbum=itemView.findViewById(R.id.tv_album);
        }
    }
}
